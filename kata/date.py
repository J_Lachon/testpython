def format(seconds):
    if not(seconds):
        return "now"
    
    ref = [31536000,86400,3600,60,1]
    refe = ["year", "day", "hour", "minute", "second"]
    sum = []
    
    for i in range(len(ref)):
        sum.append(seconds//ref[i])
        seconds %= ref[i]
    refe = [refe[x]+"s" if (sum[x] > 1) else refe[x] for x in range(len(ref))]
    sum = [(sum[i],refe[i]) for i in range(len(ref)) if (sum[i] != 0)]
    sum = [" ".join([str(i) for i in x]) for x in sum]
    sum1 = sum.pop()
    return " and ".join([", ".join(x for x in sum), sum1])


format(1239091201)