def sum_for_list(lst):
    prime = []
    for i in lst:
        prime.extend(trial_division(abs(i)))
    prime = sorted(list(set(prime)))
    res = []
    for i in prime:
        sum = 0
        for j in lst:
            if not(j%i):
                sum+=j
        res.append([i,sum])
    return res
            
def trial_division(n: int) -> list[int]:
    a = []
    while n % 2 == 0:
        a.append(2)
        n //= 2
    f = 3
    while f * f <= n:
        if n % f == 0:
            a.append(f)
            n //= f
        else:
            f += 2
    if n != 1: a.append(n)
    return a
