class RomanNumerals:
    

    def to_roman(val):
        sym = ['I','V','X','L','C','D','M']
        x = str(val)
        r = []
        e = 0
        while x!='':
            m = []
            a = int(x[-1])
            if a == 0:
                pass
            elif a < 4:
                for i in range(a):
                    m.append(sym[e])
            elif a == 4:
                m.extend([sym[e],sym[e+1]])
            elif a < 9:
                m.append(sym[e+1])
                for i in range(a-5):
                    m.append(sym[e])
            else:
                m.extend([sym[e],sym[e+2]])
            x = x[:-1]
            e+=2
            r.append("".join(m))
        r.reverse()
        return ''.join(r)

    def from_roman(roman_num):
        sym2 = ['M','D','C','L','X','V','I','','','']
        valu = [1000,500,100,50,10,5,1]
        x = roman_num
        a = 0
        sum = 0
        while x:
            if x[0]!=sym2[a]:
                a+=1
            else:
                if len(x)>1:
                    if x[1]==sym2[a-1]:
                        sum+= valu[a-1]-valu[a]
                        x=x[2:]
                    elif x[1]==sym2[a-2]:
                        sum+= valu[a-2]-valu[a]
                        x=x[2:]
                    else:
                        sum+=valu[a]
                        x=x[1:]
                else:
                    sum+=valu[a]
                    x=x[1:]
        return sum