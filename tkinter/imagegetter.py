import io
import tkinter as tk
import requests
from PIL import Image,ImageTk


root = tk.Tk() 
root.title('GarageImage') 
root.resizable(True, True)
root.geometry('500x500')


i = requests.get("https://www.legaragenumerique.fr/wp-content/uploads/2019/10/logo-web-garage_numerique.png")
img = io.BytesIO(i.content)
imge = Image.open(img)
image = ImageTk.PhotoImage(imge)
la = tk.Label(root,image=image)
la.place(x=50,y=50)

root.mainloop()

