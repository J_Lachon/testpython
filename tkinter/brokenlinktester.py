import tkinter as tk
import requests
from tkinter import messagebox
from bs4 import BeautifulSoup
from urllib.parse import urlparse


def scrap():
    global inputur, root
    inputurl = inputur.get()
    try:
        data = requests.get("https://"+inputurl).text
    except:
        messagebox.showerror(title='url error', message='URL not valid.')
    soup = BeautifulSoup(data)
    tags = soup.find_all('a')
    linktotest = [tag['href'] for tag in tags]
    liens = []
    for url in linktotest:
        if (url[:4] != "http"):
            url = "https://"+inputurl+url
        try:
            if requests.get(url).status_code==200:
                pars = urlparse(url)
                liens.append(pars.hostname+pars.path)
            else:
                raise Exception("error")
        except:
            # pars = urlparse(url)
            # liens.append(pars.hostname)
            pass
    if liens:
        messagebox.showwarning(title='not working urls',message="\n".join(liens))
    else:
        messagebox.showinfo(title='normal',message='Tous les liens marchent.')


root = tk.Tk()
root.title('broken link tester')
root.geometry('300x300')
options = {'pady':20}

etiquette = tk.Label(root, text='Input URL')
etiquette.pack(**options)
inputur = tk.Entry(root)
inputur.pack(**options)
inputur.focus_force()
btn = tk.Button(root,text='Check links',command=scrap)
btn.pack(**options)
root.bind('<Return>',lambda event:scrap())


root.mainloop()
