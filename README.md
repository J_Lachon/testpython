# Folders

* Folder kata:
    * date: input a number in seconds, return it in human format.
    * prime: input a list of integers(positive and negative), returns a list of prime factors and the sum of the prime's integers
    * romannumerals: a class containing two functions, translating a number into roman numerals and vice-versa

* Folder pythonexo:
    * classe: file containing a few classes used in main
    * main: script to test the functionalities of classe's classes
    * algo: list of small algorithm functions

* Folder webpython:
    * githapi: input a github username, returns id and repositories
    * gitlabapi: function to create a project and create a new branch and list all owned repositories (requires token)
    * webscrap: function to scrape an url for all usable links

* Folder tkinter:
    * firstwindow: tkinter window with a clickable button
    * imagegetter: tkinter window with an image
    * brokenlinktester: tkinter window to input a link and returns broken links


## Installation

Installation of required libraries

```bash
pip install -r requirements.txt
```

## Usage

```bash
python3 {folder name}/{file name}
```

