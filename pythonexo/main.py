from pythonexo.classe import Joueur, Note, Life, Humain, Poisson
jonah = Joueur("jonah", 12, 14)



jonah.revive()
jonah.get_state()
jonah.kill()
jonah.announce()

dic = Note("numb")

dic.addnote("one","o n e")
dic.addnote("three", 5)
dic.addnote("2", "five")

dic.list()
dic.find("three")

bacteria = Life(100)
jeff = Humain(30, "europe")
nemo = Poisson(10, 100)

bacteria.age()
print(bacteria.get_age())

jeff.age()
jeff.think()
print(jeff.get_place())

nemo.age()
nemo.swimdown()
print(nemo.get_age(), nemo.get_depth(), nemo.get_planet())
