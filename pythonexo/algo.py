def max(numlist):
    maximum = numlist[0]
    for nombre in numlist:
        if nombre > maximum:
            maximum = nombre
    return maximum

def max1(numlist):
    if len(numlist) == 1:
        return numlist[0] 
    if (numlist[1]<numlist[0]):
        numlist[1] = numlist[0]
    return max1(numlist[1:])


def div313(num):
#   return True if (not(num%3) and not(num%13)) else False
    return (not(num%3) and not(num%13))

def ispair(num):
    return (not(num%2))

def isvoyelle(lettre):
    '''
    if (lettre.lower()=='a'):
        return True
    elif (lettre.lower()=='e'):
        return True
    elif (lettre.lower()=='i'):
        return True
    elif (lettre.lower()=='o'):
        return True
    elif (lettre.lower()=='u'):
        return True
    else:
        return False
    '''
    return (lettre.lower() in 'aeiou')

def jourdesemaine(jour):
    """
     if jour in range(1,8):
        match jour:
            case 1:
                return 'Lundi'
            case 2:
                return 'Mardi'
            case 3:
                return 'Mercredi'
            case 4:
                return 'Jeudi'
            case 5:
                return 'Vendredi'
            case 6:
                return 'Samedi'
            case 7:
                return 'Dimanche'
            case _:
                return "Pas dans 1-7"
    """
    return ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche'][jour-1] if (jour in range(1,8)) else "Pas dans 1-7"

def nbjourdansmois(mois):
    try:
        int(mois)
        if mois in range(1,13):
            if mois in [1,3,5,7,8,10,12]:
                return 31
            elif mois == 2:
                return 28
            else:
                return 30
        else:
            return "Pas entre 1-12"
    except:
        print("Pas entre 1-12")

def profit(fabrication,vente):
    return vente-fabrication > 0, vente-fabrication

def typechar(char):
    try:
        char = str(char)[0]
        if char in '0123456789':
            return "Chiffre"
        elif char.lower() in 'abcdefghijklmnopqrstuvwxyz':
            return "Alphabet"
        else:
            return "Symbole Special"
    except:
        return "Pas un caractere"


