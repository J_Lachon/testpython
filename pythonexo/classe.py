class Joueur:
    def __init__(self,nom,vie,points):
        self.__nom = nom
        self.__vie = vie
        self.__points = points
        self.__state = True
    
    def get_nom(self):
        return self.__nom
    def get_vie(self):
        return self.__vie
    def get_points(self):
        return self.__points
    def get_state(self):
        return self.__state

    def set_nom(self,nom):
        self.__nom = nom
    def set_vie(self,vie):
        self.__vie = vie
    def set_points(self,points):
        self.__points = points
    def set_state(self,state):
        self.__state = state

    def addpoints(self,add):
        self.__points += add
    def kill(self):
        self.__state = False
        self.__vie = 0
    def revive(self):
        if self.__state:
            return    
        self.__state = True
        self.__vie = 1

    def announce(self):
        print(f"Le joueur s'appelle {self.__nom}, il a {self.__vie} points de vie et a un score de {self.__points}.")
        return



class Note:
    def __init__(self,name):
        self.__name = name
        self.__blocnote = {}
    

    def get_name(self):
        return self.__name
    def get_blocnote(self):
        return self.__blocnote


    def set_name(self,name):
        self.__name = name

    def addnote(self,title,note):
        self.__blocnote[title] = note
    
    def list(self):
        print("\n".join([x for x in self.__blocnote]))
    
    def find(self,keyname):
        try:
            print(self.__blocnote[keyname])
        except Exception as exc:
            print(f"erreur de type {exc.__class__} \nreesayer")


class Life():
    def __init__(self, age):
        self.__planet = "earth"
        self.__status = True
        self.__age = age
    
    def get_planet(self):
        return self.__planet
    def get_status(self):
        return self.__status
    def get_age(self):
        return self.__age

    def set_planet(self, planet):
        self.__planet = planet
    def set_status(self,status):
        self.__status = status
    def set_age(self, age):
        self.__age = age

    def age(self):
        self.__age += 1
    


class Humain(Life):
    def __init__(self, age, place):
        self.__place = place
        super().__init__(age)
    
    def get_place(self):
        return self.__place
    def set_place(self, place):
        self.__place = place
    
    def think(self):
        print("Eureka!")

class Poisson(Life):
    def __init__(self, age, depth):
        self.__depth = depth
        super().__init__(age)

    def get_depth(self):
        return self.__depth
    def set_depth(self, depth):
        self.__depth = depth

    def swimup(self):
        self.__depth += 10
    def swimdown(self):
        self.__depth -= 10

    
    