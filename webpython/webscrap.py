import requests
from bs4 import BeautifulSoup
from prettytable import PrettyTable
import re

def scrap():
    try:
        inputurl = input("Url du site: ")
        if (inputurl == 'exit'):
            return
        data = requests.get(f"https://{inputurl}").text
        linktotest = re.findall('href="(.+?)"',data)
        liens = []
        '''
        d = data.split("<a ")
        e = [z for x in d for z in x.split(">", 1)][::2]
        f = [z for x in e for z in x.split('href="')][::2]
        linktotest = [z for x in f for z in x.split('"')][1::2]
        '''
        for url in linktotest:
            if (url[:4] != "http"):
                url = "https:"+url
            try:
                requests.get(url)
                liens.append(url)
            except:
                pass
        table = PrettyTable()
        table.field_names = ['Numero de lien','URL']
        table._max_width = {"URL" : 60}
        table.padding_width = 5
        n = 0
        for lien in liens:
            table.add_row([f"Lien {n}", lien])
            n+=1

        print(table)
        
        
    except Exception as exc:
        print(exc)
        scrap()


def scrap1():
    try:
        inputurl = input("Url du site: ")

        # In case of infinite loop
        if (inputurl == 'exit'):
            return

        # Getting html from inputurl
        data = requests.get(f"https://{inputurl}").text

        # Souping the data and finding 'href' in <a> tags
        soup = BeautifulSoup(data)
        tags = soup.find_all('a')
        linktotest = [tag['href'] for tag in tags]

        # Defining a pretty table
        table = PrettyTable()
        table.field_names = ['Numero de lien','URL']
        table._max_width = {"URL" : 60}
        table.padding_width = 5

        # Testing out the links
        n = 0
        liens = []
        for url in linktotest:

            # for links starting with '
            # /'
            if (url[:4] != "http"):
                url = f"https://{inputurl+url}"
            
            # adding link if requests.get works
            try:
                requests.get(url)
                liens.append(url)
            except:
                pass     

        # Adding links to the table
        for lien in liens:
            table.add_row([f"Lien {n}", lien])
            n+=1


        print(table)
    
    # Recall function if input link is incorrect
    except Exception as exc:
        print(exc)
        scrap1()


scrap1()