from webpython.env import token
import gitlab

gl = gitlab.Gitlab(private_token=token)

#creates a project with name (param), and a branch named (param) in that project.
def creaproj(a,b):
    project = gl.projects.create({'name': a})
    nbranch = project.branches.create({'branch': b,'ref':'main'})

#lists all owned projects and branches of the projects.
projects = gl.projects.list(get_all=True, owned=True)
for proj in projects:
    y = proj.branches.list(get_all=True)
    print(f"Voici le projet: {proj.attributes['name']} et ses branches: {', '.join([x.attributes['name'] for x in y])}")